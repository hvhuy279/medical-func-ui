import styled from "styled-components";

export const Box = styled.div`
  /* default */
  box-shadow: 0px 13px 19px 0px rgba(0, 0, 0, 0.07);
  width: 100%;
  background-color: white;
  display: flex;
  flex-direction: column;

  /* custom */
  padding: ${(props) => props.padding};
  gap: ${(props) => props.gap};
`;
