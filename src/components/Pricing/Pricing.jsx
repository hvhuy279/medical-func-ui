import React from "react";
import "./Pricing.css";
import { Tag, Title, Desc } from "../Tag";
import { Row, Col } from "reactstrap";
import PricingItem from "./PricingItem/PricingItem";

const Pricing = () => {
  return (
    <section className="pricing">
      <Row className="pricing__content">
        <Tag>Practice Advice</Tag>
        <Title fontSize="40px" lineheight="50px">
          Pricing
        </Title>
        <Desc fontSize="14px" lineheight="20px">
          Problems trying to resolve the conflict between
          <br />
          the two major realms of Classical physics: Newtonian mechanics
        </Desc>
      </Row>

      <Row className="pricing__menu">
        <Col md="4">
          <PricingItem />
        </Col>
        <Col md="4">
          <PricingItem />
        </Col>
        <Col md="4">
          <PricingItem />
        </Col>
      </Row>
    </section>
  );
};

export default Pricing;
