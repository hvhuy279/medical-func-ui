import React from "react";
import "./PricingItem.css";
import { Box } from "../../Box";
import loveIcon from "../../../assets/icon/love-icon.svg";
import { Desc, Title } from "../../Tag";
import { ButtonStyled } from "../../ButtonStyled";
import checkIcon from "../../../assets/icon/check-icon.svg";

const PricingItem = () => {
  return (
    <Box padding="50px 40px" gap="35px 0">
      <div className="product__item--icon">
        <img src={loveIcon} alt="love-icon" />
      </div>
      <Title fontSize="24px" lineheight="32px">
        FREE
      </Title>
      <p className="product__item--param">Organize across all apps by hand</p>
      <div className="product__item--price">
        <span className="item--price">19$</span>
        <br />
        <span className="item--annualy">Per Month</span>
      </div>
      <Desc fontSize="14px" lineheight="20px">
        Slate helps you see how
        <br />
        many more days you
        <br />
        need...
      </Desc>
      <ButtonStyled
        bgcolor="#23A6F0"
        borderradius="5px"
        color="white"
        padding="15px"
      >
        Try for free
      </ButtonStyled>
      <ul className="product__item--benefits p-0 m-0">
        <li className="item--benefit">
          <img src={checkIcon} alt="check-icon" className="check-icon" />
          <Title fontSize="14px" lineheight="24px">
            Unlimited product updates
          </Title>
        </li>
        <li className="item--benefit">
          <img src={checkIcon} alt="check-icon" className="check-icon" />
          <Title fontSize="14px" lineheight="24px">
            Unlimited product updates
          </Title>
        </li>
        <li className="item--benefit">
          <img src={checkIcon} alt="check-icon" className="check-icon" />
          <Title fontSize="14px" lineheight="24px">
            Unlimited product updates
          </Title>
        </li>
        <li className="item--benefit">
          <img
            src={checkIcon}
            alt="check-icon"
            className="check-icon check-none-icon"
          />
          <Title fontSize="14px" lineheight="24px">
            1GB Cloud storage
          </Title>
        </li>
        <li className="item--benefit">
          <img
            src={checkIcon}
            alt="check-icon"
            className="check-icon check-none-icon"
          />
          <Title fontSize="14px" lineheight="24px">
            Email and community support
          </Title>
        </li>
      </ul>
    </Box>
  );
};

export default PricingItem;
