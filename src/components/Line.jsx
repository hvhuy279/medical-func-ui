import styled from "styled-components";

export const Line = styled.div`
  width: ${(props) => props.width};
  height: ${(props) => props.height};
  background-color: #e74040;
`;
