import React, { useState } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
} from "reactstrap";
import { ButtonStyled } from "../ButtonStyled";
import "./Header.css";

const Header = () => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);
  return (
    <Navbar expand="lg">
      <NavbarBrand href="/">
        <h3 className="header__logo">MedicalFunc</h3>
      </NavbarBrand>
      <NavbarToggler onClick={toggle} />
      <Collapse isOpen={isOpen} navbar>
        <Nav className="me-auto" navbar>
          <NavItem>
            <NavLink href="/">
              <span className="header__nav--item">Home</span>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink href="/product/" className="header__nav--item">
              <span className="header__nav--item">Product</span>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink href="/pricing/" className="header__nav--item">
              <span className="header__nav--item">Pricing</span>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink href="/contact/" className="header__nav--item">
              <span className="header__nav--item">Contact</span>
            </NavLink>
          </NavItem>
        </Nav>
        <button className="btn-login">Login</button>
        <ButtonStyled
          bgcolor="#23A6F0"
          borderradius="5px"
          color="white"
          padding="15px 25px"
        >
          JOIN US
          <i className="fa-solid fa-arrow-right ms-2"></i>
        </ButtonStyled>
      </Collapse>
    </Navbar>
  );
};

export default Header;
