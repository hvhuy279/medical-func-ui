import React from "react";
import { Tag, Title, Desc } from "../Tag";
import { Box } from "../Box";
import { Row, Col } from "reactstrap";
import { Line } from "../Line";
import emergencyImg from "../../assets/images/emergency.svg";
import healthImg from "../../assets/images/health.svg";
import painlessImg from "../../assets/images/painless.svg";
import "./Activity.css";

const Activity = () => {
  return (
    <section className="activity">
      <Row className="activity__content">
        <Tag>Practice Advice</Tag>
        <Title>Our Activity</Title>
        <Desc>
          Problems trying to resolve the conflict between
          <br />
          the two major realms of Classical physics: Newtonian mechanics
        </Desc>
      </Row>
      <Row className="activity__menu">
        <Col md="4">
          <Box padding="35px 40px" gap="20px">
            <img
              src={emergencyImg}
              alt="emergency-img"
              className="activity__menu--img"
            />
            <Title fontSize="16px" lineheight="24px">
              Emergency Case
            </Title>
            <Line width="50px" height="2px" />
            <Desc fontSize="14px" lineheight="20px">
              The gradual
              <br />
              accumulation of
              <br />
              information about{" "}
            </Desc>
          </Box>
        </Col>
        <Col md="4">
          <Box padding="35px 40px" gap="20px">
            <img
              src={healthImg}
              alt="health-img"
              className="activity__menu--img"
            />
            <Title fontSize="16px" lineheight="24px">
              Emergency Case
            </Title>
            <Line width="50px" height="2px" />
            <Desc fontSize="14px" lineheight="20px">
              The gradual
              <br />
              accumulation of
              <br />
              information about{" "}
            </Desc>
          </Box>
        </Col>
        <Col md="4">
          <Box padding="35px 40px" gap="20px">
            <img
              src={painlessImg}
              alt="painless-img"
              className="activity__menu--img"
            />
            <Title fontSize="16px" lineheight="24px">
              Emergency Case
            </Title>
            <Line width="50px" height="2px" />
            <Desc fontSize="14px" lineheight="20px">
              The gradual
              <br />
              accumulation of
              <br />
              information about{" "}
            </Desc>
          </Box>
        </Col>
      </Row>
    </section>
  );
};

export default Activity;
