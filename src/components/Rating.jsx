import styled from "styled-components";

export const Rating = styled.div`
  display: flex;
  padding: 5px;
  align-items: center;
  gap: 5px;
  border-radius: 20px;
  background: #252b42;
  color: white;

  /* text */
  font-family: Montserrat;
  font-size: 12px;
  font-weight: 400;
  line-height: 16px;
  letter-spacing: 0.2px;
`;
