import React from "react";
import "./FeedbackItem.css";
import { Tag, Desc } from "../../Tag";
import starIcon from "../../../assets/icon/star-yellow-icon.svg";
import starNoneIcon from "../../../assets/icon/star-none-icon.svg";

const FeedbackItem = (props) => {
  return (
    <div className="feedback-item">
      <div className="feedback-item__rating">
        <img src={starIcon} alt="star-icon" />
        <img src={starIcon} alt="star-icon" />
        <img src={starIcon} alt="star-icon" />
        <img src={starIcon} alt="star-icon" />
        <img src={starNoneIcon} alt="star-icon" />
      </div>
      <Desc className="text-center feedback-item__desc">
        Slate helps you see how many more days you need to work to reach your
        financial goal for the month and year.
      </Desc>
      <div className="feedback-item__user">
        <img src={props.userImage} alt="user-avatar" />
        <div className="feedback-item__user--info">
          <Tag>Regina Miles</Tag>
          <p className="info-job m-0">Designer</p>
        </div>
      </div>
    </div>
  );
};

export default FeedbackItem;
