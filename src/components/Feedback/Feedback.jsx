import React from "react";
import { Row, Col } from "reactstrap";
import { Tag, Title, Desc } from "../Tag";
import "./Feedback.css";
import FeedbackItem from "./FeedbackItem/FeedbackItem";
import userAvatar1 from "../../assets/images/user-1.png";
import userAvatar2 from "../../assets/images/user-2.png";
import userAvatar3 from "../../assets/images/user-3.png";

const Feedback = () => {
  return (
    <section className="feedback">
      <Row className="feedback__content">
        <Tag>Practice Advice</Tag>
        <Title fontSize="40px" lineheight="50px">
          Each and every client is important
        </Title>
        <Desc fontSize="14px" lineheight="20px">
          Problems trying to resolve the conflict between
          <br />
          the two major realms of Classical physics: Newtonian mechanics
        </Desc>
      </Row>

      <Row className="feedback__menu">
        <Col md="4">
          <FeedbackItem userImage={userAvatar1} />
        </Col>
        <Col md="4">
          <FeedbackItem userImage={userAvatar2} />
        </Col>
        <Col md="4">
          <FeedbackItem userImage={userAvatar3} />
        </Col>
      </Row>
    </section>
  );
};

export default Feedback;
