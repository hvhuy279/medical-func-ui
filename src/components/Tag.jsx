import styled from "styled-components";

export const Tag = styled.p`
  color: #23a6f0;

  /* h5 */
  font-family: Montserrat;
  font-size: 16px;
  font-weight: 700;
  line-height: 24px;
  letter-spacing: 0.2px;
  margin: 0;
`;

export const Title = styled.h2`
  color: #252b42;
  font-family: Montserrat;
  font-size: ${(props) => props.fontSize};
  font-weight: 700;
  line-height: ${(props) => props.lineheight};
  letter-spacing: 0.2px;
  margin: 0;
`;

export const Desc = styled.p`
  color: #737373;
  font-family: Montserrat;
  font-size: ${(props) => props.fontsize};
  font-style: normal;
  font-weight: 400;
  line-height: ${(props) => props.lineheight}; /* 150% */
  letter-spacing: 0.2px;
  margin: 0;
`;
