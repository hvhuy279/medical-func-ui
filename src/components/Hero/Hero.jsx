import React from "react";
import { ButtonStyled } from "../ButtonStyled";
import { Tag, Title, Desc } from "../Tag";
import { Row, Col } from "reactstrap";
import heroImage from "../../assets/images/hero-img.png";
import "./Hero.css";

const Hero = () => {
  return (
    <Row className="hero">
      <Col md="6" className="hero__content">
        <Tag className="hero__content--tag">Welcome</Tag>
        <Title
          fontSize="58px"
          lineheight="80px"
          className="hero__content--title"
        >
          Online
          <br />
          Appoinment
        </Title>
        <Desc fontSize="20px" lineheight="30px" className="hero__content--desc">
          Medical Functional is most focused in helping you discover your most
          beautiful smile
        </Desc>
        <div>
          <ButtonStyled
            bgcolor="#23A6F0"
            borderradius="5px"
            color="white"
            padding="15px 40px"
            className="btn-quote"
          >
            Get Quote Now
          </ButtonStyled>
          <ButtonStyled
            bgcolor="transparent"
            borderradius="5px"
            color="#23A6F0"
            padding="15px 40px"
          >
            Learn More
          </ButtonStyled>
        </div>
      </Col>
      <Col md="6" className="hero__image">
        <img src={heroImage} alt="hero-img" className="hero--image" />
      </Col>
    </Row>
  );
};

export default Hero;
