import React from "react";
import { Row, Col } from "reactstrap";
import { Tag, Title, Desc } from "../Tag";
import { Line } from "../Line";
import "./Medicine.css";
import medicineImg from "../../assets/images/leading-img.png";

const Medicine = () => {
  return (
    <section className="medicine">
      <Row className="align-items-center justify-content-between">
        <Col md="5" className="medicine__content">
          <Line width="94px" height="7px" />
          <Title fontSize="40px" lineheight="50px">
            Leading Medicine
          </Title>
          <Desc fontSize="14px" lineheight="20px">
            Problems trying to resolve the conflict between
            <br />
            the two major realms of Classical physics:
            <br />
            Newtonian mechanics
          </Desc>
          <a href="/" className="medicine__content--link">
            Learn More
            <i className="fa-solid fa-chevron-right ms-2"></i>
          </a>
        </Col>
        <Col md="6" className="medicine__img">
          <img src={medicineImg} alt="medicine-img" className="medicine--img" />
        </Col>
      </Row>
    </section>
  );
};

export default Medicine;
