import styled from "styled-components";

export const ButtonStyled = styled.button`
  background-color: ${(props) => props.bgcolor};
  border-radius: ${(props) => props.borderradius};
  color: ${(props) => props.color};
  padding: ${(props) => props.padding};

  /* default */
  border: 1px solid #23a6f0;
  outline: none;
  width: ${(props) => (props.max ? "max-content" : "auto")};

  /* text */
  font-family: Montserrat;
  font-size: 14px;
  font-style: normal;
  font-weight: 700;
  line-height: 22px; /* 157.143% */
  letter-spacing: 0.2px;
`;
