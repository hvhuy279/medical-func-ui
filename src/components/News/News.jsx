import React from "react";
import "./News.css";
import { Tag, Title, Desc } from "../Tag";
import { ButtonStyled } from "../ButtonStyled";
import { Col, Row } from "reactstrap";

const News = () => {
  return (
    <section className="news">
      <Row className="news__content">
        <Tag className="text-center">Newsletter</Tag>
        <Title className="text-center" fontSize="40px" lineheight="50px">
          JOIN US
        </Title>
        <Desc className="text-center" fontSize="14px" lineheight="20px">
          Problems trying to resolve the conflict between
          <br />
          the two major realms of Classical physics: Newtonian mechanics
        </Desc>
      </Row>

      <Row className="news__form">
        <Col md="8" className="news--form">
          <input
            className="form-control news--input"
            type="text"
            placeholder="Your Email"
          />
          <ButtonStyled
            bgcolor="#23A6F0"
            borderradius="0px 5px 5px 0px"
            color="white"
            padding="15px 22.5px"
          >
            Subscribe
          </ButtonStyled>
        </Col>
      </Row>
    </section>
  );
};

export default News;
