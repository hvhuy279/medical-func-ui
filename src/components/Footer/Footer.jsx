import React from "react";
import "./Footer.css";
import phoneIcon from "../../assets/icon/phone-icon.svg";
import locationIcon from "../../assets/icon/location-icon.svg";
import mailIcon from "../../assets/icon/mail-icon.svg";
import facebookIcon from "../../assets/icon/facebook-icon.svg";
import instagramIcon from "../../assets/icon/instagram-icon.svg";
import twitterIcon from "../../assets/icon/twitter-icon.svg";
import { Container, Row, Col } from "reactstrap";
import { Title } from "../Tag";

const Footer = () => {
  return (
    <footer className="footer">
      <Container>
        <Row className="pt-5 pb-5 footer__info">
          <Col md="2">
            <Title fontSize="16px" lineheight="24px" className="mb-3">
              Company Info
            </Title>
            <ul className="footer__company m-0 p-0">
              <li>
                <a href="/about">About Us</a>
              </li>
              <li>
                <a href="/carrier">Carrier</a>
              </li>
              <li>
                <a href="/hiring">We are hiring</a>
              </li>
              <li>
                <a href="/blog">Blog</a>
              </li>
            </ul>
          </Col>
          <Col md="2">
            <Title fontSize="16px" lineheight="24px" className="mb-3">
              Legal
            </Title>
            <ul className="footer__company m-0 p-0">
              <li>
                <a href="/about">About Us</a>
              </li>
              <li>
                <a href="/carrier">Carrier</a>
              </li>
              <li>
                <a href="/hiring">We are hiring</a>
              </li>
              <li>
                <a href="/blog">Blog</a>
              </li>
            </ul>
          </Col>
          <Col md="2">
            <Title fontSize="16px" lineheight="24px" className="mb-3">
              Features
            </Title>
            <ul className="footer__company m-0 p-0">
              <li>
                <a href="/business">Business Marketing</a>
              </li>
              <li>
                <a href="/analytic">User Analytic</a>
              </li>
              <li>
                <a href="/chat">Live Chat</a>
              </li>
              <li>
                <a href="/support">Unlimited Support</a>
              </li>
            </ul>
          </Col>
          <Col md="2">
            <Title fontSize="16px" lineheight="24px" className="mb-3">
              Resources
            </Title>
            <ul className="footer__company m-0 p-0">
              <li>
                <a href="/mobile">IOS & Android</a>
              </li>
              <li>
                <a href="/demo">Watch a Demo</a>
              </li>
              <li>
                <a href="/customer">Customers</a>
              </li>
              <li>
                <a href="/api">API</a>
              </li>
            </ul>
          </Col>
          <Col md="4">
            <Title fontSize="16px" lineheight="24px" className="mb-3">
              Get In Touch
            </Title>
            <ul className="footer__company m-0 p-0">
              <li>
                <a href="tel:+(480) 555-0103">
                  <img src={phoneIcon} alt="phone-icon" />
                  (480) 555-0103
                </a>
              </li>
              <li>
                <a href="/map">
                  <img src={locationIcon} alt="location-icon" />
                  4517 Washington Ave. Manchester, Kentucky 39495
                </a>
              </li>
              <li>
                <a href="/customer">
                  <img src={mailIcon} alt="mail-icon" />
                  debra.holt@example.com
                </a>
              </li>
            </ul>
          </Col>
        </Row>
      </Container>

      <div className="footer__copyright">
        <Container>
          <Row className="pt-4 pb-4">
            <Col md="6">
              <p className="m-0 footer__copyright--author">
                Made With Love By Figmaland All Right Reserved
              </p>
            </Col>
            <Col md="6">
              <div className="footer__copyright--social">
                <img src={facebookIcon} alt="facebook-icon" />
                <img src={instagramIcon} alt="instagram-icon" />
                <img src={twitterIcon} alt="twitter-icon" />
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </footer>
  );
};

export default Footer;
