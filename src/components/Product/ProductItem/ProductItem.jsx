import React from "react";
import "./ProductItem.css";
import { Card, CardBody } from "reactstrap";
import { Tag, Title, Desc } from "../../Tag";
import { Rating } from "../../Rating";
import { ButtonStyled } from "../../ButtonStyled";
import starIcon from "../../../assets/icon/star-yellow-icon.svg";
import saleIcon from "../../../assets/icon/download-icon.svg";
import clockIcon from "../../../assets/icon/clock-icon.svg";
import lessonIcon from "../../../assets/icon/chart-icon.svg";
import progressIcon from "../../../assets/icon/area-icon.svg";

const ProductItem = (props) => {
  return (
    <Card
      style={{
        width: "100%",
      }}
    >
      <img
        alt="doctor-img"
        src={props.imageDoctor}
        className="product__image"
      />
      <CardBody className="product__content">
        <div className="product__tag">
          <Tag>English Departement</Tag>
          <Rating>
            <img
              src={starIcon}
              alt="star-icon"
              className="product__tag--icon"
            />
            4.9
          </Rating>
        </div>
        <Title fontSize="16px" lineheight="24px">
          Graphic Design
        </Title>
        <Desc fontSize="14px" lineheight="20px">
          We focus on ergonomics and meeting you where you work. It's only a
          keystroke away.
        </Desc>
        <div className="product__sale">
          <img src={saleIcon} alt="sale-icon" />
          15 Sales
        </div>
        <div className="product__price">
          <span>$16.48</span>
          <span className="product__price--disc">$6.48</span>
        </div>
        <div className="product__process">
          <div className="product__process--duration">
            <img src={clockIcon} alt="clock-icon" />
            22hr 30min
          </div>
          <div className="product__process--lesson">
            <img src={lessonIcon} alt="lesson-icon" />
            64 Lessons
          </div>
          <div className="product__process--progress">
            <img src={progressIcon} alt="progress-icon" />
            Progress
          </div>
        </div>
        <ButtonStyled
          bgcolor="transparent"
          borderradius="37px"
          color="#23A6F0"
          padding="10px 20px"
          max="true"
        >
          Learn More
          <i className="fa-solid fa-chevron-right ms-2"></i>
        </ButtonStyled>
      </CardBody>
    </Card>
  );
};

export default ProductItem;
