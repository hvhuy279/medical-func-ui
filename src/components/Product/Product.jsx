import React from "react";
import "./Product.css";
import { Row, Col } from "reactstrap";
import { Tag, Title, Desc } from "../Tag";
import doctorImage1 from "../../assets/images/doctor-img.jpg";
import doctorImage2 from "../../assets/images/doctor2-img.jpg";
import doctorImage3 from "../../assets/images/doctor3-img.jpg";
import ProductItem from "./ProductItem/ProductItem";

const Product = () => {
  return (
    <section className="product">
      <Row className="product__content" style={{ marginBottom: "80px" }}>
        <Tag>Practice Advice</Tag>
        <Title fontSize="40px" lineheight="50px">
          Our Doctor Specialize in you
        </Title>
        <Desc fontSize="14px" lineheight="20px">
          Problems trying to resolve the conflict between
          <br />
          the two major realms of Classical physics: Newtonian mechanics
        </Desc>
      </Row>

      <Row className="product__menu">
        <Col md="4">
          <ProductItem imageDoctor={doctorImage1} />
        </Col>
        <Col md="4">
          <ProductItem imageDoctor={doctorImage2} />
        </Col>
        <Col md="4">
          <ProductItem imageDoctor={doctorImage3} />
        </Col>
      </Row>
    </section>
  );
};

export default Product;
