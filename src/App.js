import Header from "./components/Header/Header";
import "bootstrap/dist/css/bootstrap.min.css";
import Hero from "./components/Hero/Hero";
import { Container } from "reactstrap";
import "./App.css";
import rectangleLeft from "./assets/images/rectangle-left.svg";
import rectangleRight from "./assets/images/rectangle-right.svg";
import Activity from "./components/Activity/Activity";
import Medicine from "./components/Medicine/Medicine";
import Product from "./components/Product/Product";
import Pricing from "./components/Pricing/Pricing";
import Feedback from "./components/Feedback/Feedback";
import News from "./components/News/News";
import Footer from "./components/Footer/Footer";

function App() {
  return (
    <>
      <Container className="app">
        <Header />
        <Hero />
      </Container>
      <img
        src={rectangleLeft}
        alt="rectangle-left"
        className="rectangle-img rectangle-left"
      />
      <img
        src={rectangleRight}
        alt="rectangle-right"
        className="rectangle-img rectangle-right"
      />
      <Container>
        <Activity />
      </Container>

      <Container fluid className="p-0" style={{ backgroundColor: "#FFDCD1" }}>
        <Container>
          <Medicine />
        </Container>
      </Container>

      <Container>
        <Product />
        <Pricing />
        <Feedback />
        <News />
      </Container>
      <Footer />
    </>
  );
}

export default App;
